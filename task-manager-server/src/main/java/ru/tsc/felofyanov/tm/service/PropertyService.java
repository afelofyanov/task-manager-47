package ru.tsc.felofyanov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.service.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "13425";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "5566677234";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    private static final String SESSION_KEY = "session.key";

    @NotNull
    private static final String SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull
    private static final String DATABASE_USER_NAME_KEY = "database.username";

    @NotNull
    private static final String DATABASE_USER_PASSWORD_KEY = "database.password";

    @NotNull
    private static final String DATABASE_URL_KEY = "database.url";

    @NotNull
    private static final String DATABASE_URL_DEFAULT = "jdbc:postgresql://localhost:5432/taskmanager";

    @NotNull
    private static final String DATABASE_DRIVER = "database.driver";

    @NotNull
    private static final String DATABASE_DIALECT = "database.sql_dialect";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO = "database.hbm2ddl_auto";

    @NotNull
    private static final String DATABASE_SHOW_SQL = "database.show_sql";

    @NotNull
    private static final String DATABASE_FORMAT_SQL = "database.format_sql";

    @NotNull
    private static final String DATABASE_USE_SECOND_L2_CACHE = "database.cache.use_second_level_cache";

    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE = "database.cache.use_query_cache";

    @NotNull
    private static final String DATABASE_USE_MINIMAL_PUTS = "database.cache.use_minimal_puts";

    @NotNull
    private static final String DATABASE_REGION_PREFIX = "database.cache.region_prefix";

    @NotNull
    private static final String DATABASE_PROVIDER_CONFIGURATION_FILE_RESOURCE_PATH
            = "database.cache.provider_configuration_file_resource_path";

    @NotNull
    private static final String DATABASE_REGION_FACTORY_CLASS = "database.cache.region.factory_class";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();


    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String value = getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @NotNull final String value = getStringValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        @NotNull final String value = getStringValue(SESSION_TIMEOUT_KEY, EMPTY_VALUE);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getDatabaseUserName() {
        return getStringValue(DATABASE_USER_NAME_KEY, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseUserPassword() {
        return getStringValue(DATABASE_USER_PASSWORD_KEY, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return getStringValue(DATABASE_URL_KEY, DATABASE_URL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getStringValue(DATABASE_DRIVER, EMPTY_VALUE);
    }


    @NotNull
    @Override
    public String getDatabaseDialect() {
        return getStringValue(DATABASE_DIALECT, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseHbm2ddlAuto() {
        return getStringValue(DATABASE_HBM2DDL_AUTO, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseShowSql() {
        return getStringValue(DATABASE_SHOW_SQL, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseFormatSql() {
        return getStringValue(DATABASE_FORMAT_SQL, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseUseSecondL2Cache() {
        return getStringValue(DATABASE_USE_SECOND_L2_CACHE, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseUseQueryCache() {
        return getStringValue(DATABASE_USE_QUERY_CACHE, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseUseMinimalPuts() {
        return getStringValue(DATABASE_USE_MINIMAL_PUTS, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseRegionPrefix() {
        return getStringValue(DATABASE_REGION_PREFIX, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseProviderConfigurationFileResourcePath() {
        return getStringValue(DATABASE_PROVIDER_CONFIGURATION_FILE_RESOURCE_PATH, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseRegionFactoryClass() {
        return getStringValue(DATABASE_REGION_FACTORY_CLASS, EMPTY_VALUE);
    }
}
