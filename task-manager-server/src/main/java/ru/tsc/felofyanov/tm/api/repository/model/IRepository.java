package ru.tsc.felofyanov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(@Nullable M model);

    Collection<M> add(@Nullable Collection<M> collection);

    @NotNull
    List<M> findAll();

    void clear();

    boolean existsById(@Nullable String id);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @Nullable
    M findOneById(@Nullable String id);

    M findOneByIndex(@Nullable Integer index);

    M remove(@NotNull M model);

    void removeAll(@Nullable Collection<M> collection);

    M removeById(@Nullable String id);

    M removeByIndex(@Nullable Integer index);

    M update(@NotNull M model);

    long count();
}


