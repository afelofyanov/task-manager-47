package ru.tsc.felofyanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.felofyanov.tm.api.service.IConnectionService;
import ru.tsc.felofyanov.tm.api.service.dto.IProjectDTOService;
import ru.tsc.felofyanov.tm.repository.dto.ProjectDTORepository;

import javax.persistence.EntityManager;

public class ProjectDTOService extends AbstractUserOwnedDTO<ru.tsc.felofyanov.tm.dto.model.ProjectDTO, IProjectDTORepository>
        implements IProjectDTOService {

    public ProjectDTOService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected IProjectDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDTORepository(entityManager);
    }
}
