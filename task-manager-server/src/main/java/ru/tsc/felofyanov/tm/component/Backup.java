package ru.tsc.felofyanov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, 3000, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

    public void save() {
        bootstrap.getDomainService().saveDataBackup();
    }

    @SneakyThrows
    public void load() {
        bootstrap.getDomainService().loadDataBackup();
    }
}
