package ru.tsc.felofyanov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectUpdateByIdResponse extends AbstractProjectResponse {

    public ProjectUpdateByIdResponse(@Nullable ProjectDTO project) {
        super(project);
    }
}
