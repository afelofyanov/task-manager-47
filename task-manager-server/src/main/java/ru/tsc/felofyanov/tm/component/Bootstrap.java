package ru.tsc.felofyanov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.api.endpoint.*;
import ru.tsc.felofyanov.tm.api.repository.IDomainRepository;
import ru.tsc.felofyanov.tm.api.service.*;
import ru.tsc.felofyanov.tm.api.service.dto.IProjectDTOService;
import ru.tsc.felofyanov.tm.api.service.dto.IProjectTaskDTOService;
import ru.tsc.felofyanov.tm.api.service.dto.ITaskDTOService;
import ru.tsc.felofyanov.tm.api.service.dto.IUserDTOService;
import ru.tsc.felofyanov.tm.endpoint.*;
import ru.tsc.felofyanov.tm.repository.DomainRepository;
import ru.tsc.felofyanov.tm.service.*;
import ru.tsc.felofyanov.tm.service.dto.ProjectDTOService;
import ru.tsc.felofyanov.tm.service.dto.ProjectTaskDTOService;
import ru.tsc.felofyanov.tm.service.dto.TaskDTOService;
import ru.tsc.felofyanov.tm.service.dto.UserDTOService;
import ru.tsc.felofyanov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @Getter
    @NotNull
    private final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskDTOService projectTaskService = new ProjectTaskDTOService(projectService, taskService);
    @Getter
    @NotNull
    private final IUserDTOService userService = new UserDTOService(connectionService, propertyService);
    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);
    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @Getter
    @NotNull
    private final IDomainRepository domainRepository = new DomainRepository(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(domainRepository);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    {
        registry(systemEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(projectTaskEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
        registry(authEndpoint);
    }

    public void registry(Object endpoint) {
        @NotNull final String host = "localhost";
        @NotNull final String port = "8080";
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void run() {
        initPID();
        loggerService.info("** TASK-MANAGER SERVER STARTED **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER SERVER STOPPED **");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }
}
