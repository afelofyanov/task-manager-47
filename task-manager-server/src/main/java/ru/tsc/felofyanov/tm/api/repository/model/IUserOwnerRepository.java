package ru.tsc.felofyanov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.model.AbstractWbs;
import ru.tsc.felofyanov.tm.model.User;

import java.util.List;

public interface IUserOwnerRepository<M extends AbstractWbs> extends IRepository<M> {

    @Nullable
    M create(@Nullable User user, @Nullable String name);

    @Nullable
    M create(@Nullable User user, @Nullable String name, @Nullable String description);

    @NotNull
    List<M> findAllByUserId(@Nullable String userId);

    void clearByUserId(@NotNull String userId);

    boolean existsByIdUserId(@NotNull String userId, @NotNull String id);

    @Nullable
    M findOneByIdUserId(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndexByUserId(@Nullable String userId, @Nullable Integer index);

    M remove(@Nullable String userId, @Nullable M model);

    M removeByIdByUserId(@Nullable String userId, @Nullable String id);

    M removeByIndexByUserId(@Nullable String userId, @Nullable Integer index);

    long countByUserId(@Nullable String userId);
}
