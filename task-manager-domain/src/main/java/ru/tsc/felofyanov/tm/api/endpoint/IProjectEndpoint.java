package ru.tsc.felofyanov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.felofyanov.tm.dto.request.*;
import ru.tsc.felofyanov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IProjectEndpoint extends IEndpoint {

    @NotNull
    String NAME = "ProjectEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, NAMESPACE, PART, IProjectEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IProjectEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, NAMESPACE, PART, IProjectEndpoint.class);
    }

    @NotNull
    @WebMethod
    ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectChangeStatusByIdRequest request
    );

    @NotNull
    @WebMethod
    ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectChangeStatusByIndexRequest request
    );

    @NotNull
    @WebMethod
    ProjectClearResponse clearProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectClearRequest request
    );

    @NotNull
    @WebMethod
    ProjectCreateResponse createProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectCreateRequest request
    );

    @NotNull
    @WebMethod
    ProjectGetByIdResponse getProjectById(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectGetByIdRequest request
    );

    @NotNull
    @WebMethod
    ProjectGetByIndexResponse getProjectByIndex(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectGetByIndexRequest request
    );

    @NotNull
    @WebMethod
    ProjectListResponse listProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectListRequest request
    );

    @NotNull
    @WebMethod
    ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectRemoveByIdRequest request
    );

    @NotNull
    @WebMethod
    ProjectRemoveByIndexResponse removeProjectByIndex(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectRemoveByIndexRequest request
    );

    @NotNull
    @WebMethod
    ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectUpdateByIdRequest request
    );

    @NotNull
    @WebMethod
    ProjectUpdateByIndexResponse updateProjectByIndex(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectUpdateByIndexRequest request
    );
}
