package ru.tsc.felofyanov.tm.api.service.dto;

import ru.tsc.felofyanov.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.felofyanov.tm.dto.model.AbstractModelDTO;

public interface IServiceDTO<M extends AbstractModelDTO> extends IRepositoryDTO<M> {

}
