package ru.tsc.felofyanov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.felofyanov.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.felofyanov.tm.api.service.IConnectionService;
import ru.tsc.felofyanov.tm.api.service.dto.ITaskDTOService;
import ru.tsc.felofyanov.tm.exception.field.TaskIdEmptyException;
import ru.tsc.felofyanov.tm.exception.field.UserIdEmptyException;
import ru.tsc.felofyanov.tm.repository.dto.TaskDTORepository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

public class TaskDTOService extends AbstractUserOwnedDTO<ru.tsc.felofyanov.tm.dto.model.TaskDTO, ITaskDTORepository> implements ITaskDTOService {

    public TaskDTOService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ITaskDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskDTORepository(entityManager);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<ru.tsc.felofyanov.tm.dto.model.TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(projectId).orElseThrow(TaskIdEmptyException::new);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final ITaskDTORepository repository = getRepository(entityManager);
            return repository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public final void removeAllByProjectId(@Nullable final String userId, @NotNull final String projectId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final ITaskDTORepository repository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeAllByProjectId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }
}
